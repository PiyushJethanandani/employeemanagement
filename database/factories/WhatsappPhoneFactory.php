<?php

namespace Database\Factories;

use App\Models\WhatsappPhone;
use Illuminate\Database\Eloquent\Factories\Factory;

class WhatsappPhoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WhatsappPhone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'phone' => $this->faker->phoneNumber,
            'isPrimary' => rand(0, 1),
        ];
    }
}
