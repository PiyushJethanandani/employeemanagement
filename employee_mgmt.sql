-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2021 at 09:10 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee_mgmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suburb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taluka` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `employee_id`, `address`, `zip`, `suburb`, `location`, `taluka`, `city`, `district`, `state`, `country`, `created_at`, `updated_at`) VALUES
(1, 1, '8019 Gino Mountains\nSouth Duanestad, MA 38660-0842', '81269', 'Et qui neque omnis.', 'Id est sequi.', 'Consequatur.', 'Rutherfordmouth', 'Repudiandae natus.', 'Wyoming', 'India', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(2, 1, '12856 Lang Points\nCollinsborough, MI 77544', '78264', 'Quam maxime rerum.', 'Corporis rerum.', 'Officiis fugiat a.', 'Nikkiberg', 'Dicta et qui.', 'California', 'China', '2021-06-21 14:45:06', '2021-06-21 14:45:06'),
(3, 1, '478 Hand Expressway\nNorth Orionview, NY 32184', '19621', 'Inventore impedit.', 'Fuga possimus.', 'Molestias atque non.', 'Schulistbury', 'Voluptatum deserunt.', 'Washington', 'Lebanon', '2021-06-21 14:45:06', '2021-06-21 14:45:06'),
(4, 2, '7889 Doris Shoals\nNorth Marjory, FL 68631', '44601', 'Excepturi id qui.', 'Dolores distinctio.', 'Aut aut error.', 'West Ethanshire', 'Ipsam cumque id at.', 'Connecticut', 'Bahrain', '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(5, 2, '445 Daren Haven Apt. 578\nPort Austen, AR 86714-7659', '45157', 'Dolorum incidunt.', 'Tempora dolorem.', 'Enim modi numquam.', 'North Vestaport', 'Nihil perferendis.', 'Illinois', 'Slovakia (Slovak Republic)', '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(6, 2, '258 Ona Radial Suite 097\nEast Nikoton, WV 63251', '78231', 'Quidem ducimus.', 'Error vero omnis.', 'Nobis architecto.', 'West Kenna', 'Non asperiores.', 'Mississippi', 'Papua New Guinea', '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(7, 3, '7123 Dimitri Crossing Apt. 524\nJudyside, FL 55368', '77315', 'Tempore ullam.', 'Expedita ratione.', 'Alias est ratione.', 'West Raphael', 'Qui numquam.', 'District of Columbia', 'Peru', '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(8, 3, '25517 Lynch Spur\nNorth Ryder, UT 35968', '56884', 'Autem consequatur.', 'Quia asperiores et.', 'Porro et iste.', 'Fabiolaside', 'Qui consequatur.', 'California', 'Belarus', '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(9, 4, '129 Ledner Road\nBechtelarport, WY 00503-6030', '55628', 'Quae voluptates.', 'Repellendus error.', 'Hic ut rem illum.', 'Fredstad', 'Et eaque eligendi.', 'Pennsylvania', 'Denmark', '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(10, 5, '380 Roob Course\nWest Shawnatown, WI 69225-1406', '71338', 'Qui adipisci.', 'Distinctio et.', 'Autem animi et.', 'Monabury', 'Itaque voluptatem.', 'Mississippi', 'United States of America', '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(11, 5, '1594 Elnora Trail Apt. 862\nUptonfort, OH 15236', '96481', 'Non dicta eius.', 'Dolores consequatur.', 'Ut reprehenderit.', 'Tomside', 'Iure ut accusantium.', 'Indiana', 'Monaco', '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(12, 6, '706 Pagac Summit Apt. 196\nPort Garnetland, NH 13828', '60420', 'Minima blanditiis.', 'Natus est rerum.', 'In fugiat ex ipsam.', 'West Kayli', 'Assumenda earum.', 'Arizona', 'Turkmenistan', '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(13, 7, '838 Hirthe Landing\nMarianashire, RI 17914-7903', '28836', 'Quo voluptatum.', 'Iste voluptas rerum.', 'Dolorem non.', 'Ceciletown', 'In et sit aut.', 'California', 'Guernsey', '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(14, 7, '63903 Cormier Tunnel\nNew Elsafort, WI 42837-1684', '99318', 'Soluta quod.', 'Tenetur aut cumque.', 'Consectetur.', 'Laurianehaven', 'Consequatur magnam.', 'Idaho', 'Senegal', '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(15, 8, '578 Runolfsdottir Roads\nEast Cheyanne, UT 22227', '73588', 'Omnis alias ducimus.', 'Quia a.', 'Nihil dolores.', 'Altenwerthfort', 'Nisi facilis.', 'New Hampshire', 'Dominica', '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(16, 9, '42621 Amaya Lodge\nLake Calista, MA 23918-5049', '42165', 'Ea quia vero.', 'Sit consequatur.', 'Adipisci et iusto.', 'North Idellfurt', 'Nemo impedit.', 'Nebraska', 'Pakistan', '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(17, 9, '146 Ross Trail Suite 306\nPort Sandrineside, GA 18371', '55365', 'Provident aut harum.', 'Recusandae fuga.', 'Dolores provident.', 'Jacinthechester', 'Saepe in et.', 'Kentucky', 'South Georgia and the South Sandwich Islands', '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(18, 10, '116 Zion Land\nEast Destiny, NV 12814', '96379', 'Enim quod dolores.', 'Odit odit velit.', 'Quos delectus et et.', 'Arielleport', 'Ipsam est aut.', 'Minnesota', 'Trinidad and Tobago', '2021-06-21 14:45:13', '2021-06-21 14:45:13'),
(19, 10, '42101 Ines Isle Apt. 866\nNorth Hobartstad, MD 19182', '59245', 'Amet praesentium.', 'Perspiciatis nisi.', 'Voluptatem sequi.', 'North Tyrique', 'Ut culpa quod.', 'New Mexico', 'Macao', '2021-06-21 14:45:13', '2021-06-21 14:45:13'),
(20, 10, '457 Sarai Crest\nSouth Antoinettebury, ND 36554', '18133', 'Tempora eos ipsa.', 'Qui laboriosam.', 'Necessitatibus.', 'Erdmanfurt', 'Modi explicabo.', 'Pennsylvania', 'Macedonia', '2021-06-21 14:45:14', '2021-06-21 14:45:14');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `employee_id`, `email`, `created_at`, `updated_at`) VALUES
(1, 1, 'elinore83@simonis.biz', '2021-06-21 14:45:06', '2021-06-21 14:45:06'),
(2, 1, 'webster70@ziemann.com', '2021-06-21 14:45:06', '2021-06-21 14:45:06'),
(3, 2, 'cornelius.deckow@terry.org', '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(4, 2, 'jalon91@kirlin.com', '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(5, 3, 'aleen88@yahoo.com', '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(6, 3, 'zoie.cassin@gmail.com', '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(7, 4, 'alvina77@gmail.com', '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(8, 4, 'jaylen78@wilderman.org', '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(9, 5, 'lennie.hermiston@yahoo.com', '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(10, 6, 'quinton.greenholt@hill.info', '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(11, 6, 'lulu.gibson@hotmail.com', '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(12, 6, 'ansel23@muller.com', '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(13, 7, 'mharris@hoeger.org', '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(14, 7, 'gulgowski.hester@hotmail.com', '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(15, 7, 'aheller@keeling.com', '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(16, 8, 'ortiz.leo@quigley.com', '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(17, 8, 'lbailey@stamm.com', '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(18, 8, 'cboyer@armstrong.net', '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(19, 9, 'bernhard.naomi@lockman.com', '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(20, 9, 'yundt.herman@ryan.org', '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(21, 10, 'ullrich.ivory@metz.biz', '2021-06-21 14:45:14', '2021-06-21 14:45:14'),
(22, 10, 'isai51@gmail.com', '2021-06-21 14:45:15', '2021-06-21 14:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Rosalinda Haley', '2021-06-21 14:45:04', '2021-06-21 14:45:04'),
(2, 'Carmine Herman', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(3, 'Emile Oberbrunner', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(4, 'Mr. Humberto Howell', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(5, 'Prof. Desmond Boehm', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(6, 'Prof. Reinhold Tromp', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(7, 'Rosemarie Bailey', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(8, 'Terrance Sipes', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(9, 'Giuseppe Murazik', '2021-06-21 14:45:05', '2021-06-21 14:45:05'),
(10, 'Mrs. Dortha Bayer', '2021-06-21 14:45:05', '2021-06-21 14:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_18_093132_create_employees_table', 1),
(5, '2021_06_18_093210_create_addresses_table', 1),
(6, '2021_06_18_093245_create_emails_table', 1),
(7, '2021_06_18_093330_create_phones_table', 1),
(8, '2021_06_21_021056_create_whatsapp_phones_table', 1),
(9, '2021_07_21_112445_add_verfication_fields_to_user_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPrimary` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `employee_id`, `phone`, `isPrimary`, `created_at`, `updated_at`) VALUES
(1, 1, '+1-651-376-0702', 0, '2021-06-21 14:45:06', '2021-06-21 14:45:06'),
(2, 1, '(678) 816-0193', 1, '2021-06-21 14:45:06', '2021-06-21 14:45:06'),
(3, 1, '+1-929-357-3061', 1, '2021-06-21 14:45:06', '2021-06-21 14:45:06'),
(4, 2, '+1-585-236-1940', 0, '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(5, 3, '956.380.9259', 1, '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(6, 4, '629.323.0956', 1, '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(7, 4, '410.582.8497', 1, '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(8, 4, '410.812.8679', 1, '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(9, 5, '253.754.7896', 0, '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(10, 6, '+12832504950', 0, '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(11, 6, '+1-516-889-6713', 0, '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(12, 6, '+1-913-221-1773', 0, '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(13, 7, '+1-575-433-7853', 1, '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(14, 8, '+18434391968', 1, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(15, 8, '(321) 544-2995', 1, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(16, 8, '229-530-3044', 0, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(17, 9, '+1.517.994.9511', 0, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(18, 9, '+1.269.352.9258', 1, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(19, 9, '480.246.4472', 0, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(20, 10, '1-231-845-4885', 1, '2021-06-21 14:45:15', '2021-06-21 14:45:15'),
(21, 10, '+1.516.422.4764', 0, '2021-06-21 14:45:15', '2021-06-21 14:45:15'),
(22, 10, '617-740-8653', 1, '2021-06-21 14:45:15', '2021-06-21 14:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verification_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `whatsapp_phones`
--

CREATE TABLE `whatsapp_phones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPrimary` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `whatsapp_phones`
--

INSERT INTO `whatsapp_phones` (`id`, `employee_id`, `phone`, `isPrimary`, `created_at`, `updated_at`) VALUES
(1, 1, '1-636-774-3181', 1, '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(2, 1, '862.336.2548', 0, '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(3, 1, '678-469-3550', 0, '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(4, 2, '(234) 393-4598', 0, '2021-06-21 14:45:07', '2021-06-21 14:45:07'),
(5, 2, '(716) 822-4771', 0, '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(6, 2, '+1 (872) 695-2917', 1, '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(7, 3, '1-458-960-1087', 1, '2021-06-21 14:45:08', '2021-06-21 14:45:08'),
(8, 3, '737-337-3445', 0, '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(9, 4, '(570) 737-2761', 0, '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(10, 5, '(313) 618-2348', 0, '2021-06-21 14:45:09', '2021-06-21 14:45:09'),
(11, 6, '(231) 285-3102', 1, '2021-06-21 14:45:10', '2021-06-21 14:45:10'),
(12, 6, '205-879-7777', 0, '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(13, 7, '+15807624651', 0, '2021-06-21 14:45:11', '2021-06-21 14:45:11'),
(14, 8, '(646) 285-1769', 1, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(15, 9, '(602) 417-1750', 1, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(16, 9, '863-266-0447', 0, '2021-06-21 14:45:12', '2021-06-21 14:45:12'),
(17, 10, '380-731-7266', 1, '2021-06-21 14:45:15', '2021-06-21 14:45:15'),
(18, 10, '1-319-458-8734', 1, '2021-06-21 14:45:15', '2021-06-21 14:45:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emails_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phones_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `whatsapp_phones`
--
ALTER TABLE `whatsapp_phones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `whatsapp_phones_employee_id_foreign` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `whatsapp_phones`
--
ALTER TABLE `whatsapp_phones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `emails`
--
ALTER TABLE `emails`
  ADD CONSTRAINT `emails_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `phones`
--
ALTER TABLE `phones`
  ADD CONSTRAINT `phones_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `whatsapp_phones`
--
ALTER TABLE `whatsapp_phones`
  ADD CONSTRAINT `whatsapp_phones_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
