<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function addresses() {
        return $this->hasMany(Address::class);
    }

    public function emails() {
        return $this->hasMany(Email::class);
    }

    public function phones() {
        return $this->hasMany(Phone::class);
    }

    public function whatsappPhones() {
        return $this->hasMany(WhatsappPhone::class);
    }
}
