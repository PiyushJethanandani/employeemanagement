<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsappPhone extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'phone',
        'isPrimary',
    ];

    public function employee() {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
